# USAGE
# python3 oraculo.py --prototxt deploy.prototxt.txt --model res10_300x300_ssd_iter_140000.caffemodel

# import the necessary packages

from imutils.video import VideoStream
import numpy as np
import argparse
#import freenect
import imutils
import time
import random
import cv2
from skimage.metrics import structural_similarity

def areSimilarImages(imgA, imgB):
    grayA = cv2.cvtColor(imgA, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(imgB, cv2.COLOR_BGR2GRAY)

    (score, diff) = structural_similarity(grayA, grayB, full=True)
    return True if score > 0.6 else False

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required=True,
    help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
    help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
    help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

cursos = {
"Administracao ": (0, 0, 179),
"Agronomia ": (36, 143, 36),
"Arquitetura e Urbanismo ": (0, 0, 179),
"Artes Visuais ": (0, 0, 179),
"Biomedicina ": (255, 209, 26),
"Ciencias Biologicas ": (255, 209, 26),
"Ciencia da Computacao ": (204, 0, 0),
"Ciencias Contabeis ": (0, 0, 179),
"Ciencias Economicas ": (0, 0, 179),
"Ciencias Sociais ": (0, 0, 179),
"Design Grafico ": (153, 0, 153),
"Design de Produto ": (153, 0, 153),
"Direito ": (153, 0, 153),
"Educacao Fisica ": (0, 0, 179),
"Enfermagem ": (0, 0, 179),
"Eng. Ambiental ": (36, 143, 36),
"Eng. de Bioprocessos ": (36, 143, 36),
"Eng. Cartografica ": (0, 0, 179),
"Eng. Civil ": (204, 0, 0),
"Eng. Eletrica ": (204, 0, 0),
"Eng. Florestal ": (36, 143, 36),
"Eng. Indust. Madeireira ": (36, 143, 36),
"Eng. Mecanica ": (204, 0, 0),
"Eng. de Producao ": (204, 0, 0),
"Eng. Quimica ": (204, 0, 0),
"Estatistica ": (204, 0, 0),
"Expressao Grafica ": (153, 0, 153),
"Informatica Biomedica ": (255, 209, 26),
"Farmacia ": (36, 143, 36),
"Filosofia ": (0, 0, 179),
"Fisica ": (204, 0, 0),
"Fisioterapia ": (0, 0, 179),
"Geografia ": (0, 0, 179),
"Geologia ": (0, 0, 179),
"Gestao da Informacao ": (0, 0, 179),
"Historia ": (0, 0, 179),
"Jornalismo ": (0, 0, 179),
"Letras ": (0, 0, 179),
"Letras Libras ": (0, 0, 179),
"Matematica ": (204, 0, 0),
"Matematica Industrial ": (204, 0, 0),
"Medicina ": (153, 0, 153),
"Med. Veterinaria ": (153, 0, 153),
"Musica ": (153, 0, 153),
"Nutricao ": (153, 0, 153),
"Odontologia ": (153, 0, 153),
"Pedagogia ": (0, 0, 179),
"Psicologia ": (153, 0, 153),
"Publicidade e Propaganda ": (153, 0, 153),
"Quimica ": (204, 0, 0),
"Relacoes Publicas ": (0, 0, 179),
"Tec. Desenv. de Sistemas ": (0, 0, 179),
"Tec. Com. Institucional ": (0, 0, 179),
"Tec. Gestao Publica ": (0, 0, 179),
"Tec. Gestao da Qualidade ": (0, 0, 179),
"Tec. Luteria ": (0, 0, 179),
"Tec. Negocios Imobiliarios ": (0, 0, 179),
"Tec. Producao Cenica ": (0, 0, 179),
"Tec. Secretariado ": (0, 0, 179),
"Terapia Ocupacional ": (0, 0, 179),
"Turismo ": (153, 0, 153),
"Zootecnia ": (36, 143, 36),
}

random_course = random.choice(list(cursos))

# load our serialized model from disk
print("Made by PET Computacao UFPR...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# initialize the video stream and allow the cammera sensor to warmup
print("Spawnando Oraculo...")
vs = VideoStream(src=0).start()
time.sleep(2.0)
cv2.namedWindow("Calibration", cv2.WND_PROP_FULLSCREEN)

# initialize detected faces dict 
faces = {}

# loop over the frames from the video stream
while True:
    # grab the frame from the threaded video stream and resize it
    # to have a maximum width of 400 pixels
    frame = vs.read()
    frame = imutils.resize(frame, width=1220)
    # grab the frame dimensions and convert it to a blob
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
        (300, 300), (104.0, 177.0, 123.0))
 
    # pass the blob through the network and obtain the detections and
    # predictions
    net.setInput(blob)
    detections = net.forward()
    # loop over the detections
    for i in range(0, detections.shape[2]):
        name = random_course
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence < args["confidence"]:
            continue

        # compute the (x, y)-coordinates of the bounding box for the
        # object
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
 
        # crop the face
        if (startY <= 0 or endY <= 0 or startX <= 0 or endX <= 0):
            continue

        cur_face = frame[startY:endY, startX:endX]
        cur_face = cv2.resize(cur_face, (300, 300))
        cur_face = np.ascontiguousarray(cur_face)
        cur_face_bytes = cur_face.tobytes()

        foundFace = False
        if not faces:
            course = random.choice(list(cursos))
            faces[cur_face_bytes] = course
        else:
            for face in list(faces):
                # difference = cv2.subtract(np.frombuffer(face, dtype=cur_face.dtype).reshape(cur_face.shape), np.frombuffer(cur_face_bytes, dtype=cur_face.dtype).reshape(cur_face.shape))
                # b, g, r = cv2.split(difference)
                # if (cv2.countNonZero(b) < 100 and cv2.countNonZero(g) < 100 and cv2.countNonZero(r) < 100):
                if (areSimilarImages(np.frombuffer(face, dtype=cur_face.dtype).reshape(cur_face.shape), np.frombuffer(cur_face_bytes, dtype=cur_face.dtype).reshape(cur_face.shape))):
                    course = faces.pop(face)
                    faces[cur_face_bytes] = course
                    foundFace = True
                    break
                    
        if not foundFace:
            course = random.choice(list(cursos))
            faces[cur_face_bytes] = course
                

        # draw the bounding box of the face along with the associated
        # probability

        text = course.format(confidence * 100)
        cv2.rectangle(frame, (startX - 27, endY + 40), (endX + 27, endY+1), cursos[text], cv2.FILLED)
        y = startY - 10 if startY - 10 > 10 else startY + 10
        cv2.rectangle(frame, (startX - 27, startY - 35), (endX + 27, endY),
            (204, 204, 0), 1)
        cv2.putText(frame, text, (startX, y),
            cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

    # show the output frame
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF
 
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

# do a bit of cleanup
cv2.destroyAllWindows()